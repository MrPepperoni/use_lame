#ifndef INCLUDED_MP3_DETAIL_ILAME_H
#define INCLUDED_MP3_DETAIL_ILAME_H

#include <memory>
#include <stddef.h>

namespace mp3 {
namespace detail {

class ilame
{
public:
    virtual ~ilame();

    virtual int set_num_channels(int channels) = 0;
    virtual int set_num_samples(unsigned long samples) = 0;
    virtual int set_in_samplerate(int samplerate) = 0;
    virtual int set_brate(int brate) = 0;
    virtual int set_quality(int quality) = 0;
    virtual void set_write_id3tag_automatic(bool automatic) = 0;
    virtual int init_params() = 0;
    virtual int encode_buffer_ieee_float(
        const float     pcm_l [],          /* PCM data for left channel     */
        const float     pcm_r [],          /* PCM data for right channel    */
        const int       nsamples,
        unsigned char * mp3buf,
        const int       mp3buf_size) = 0;
    virtual int encode_flush(unsigned char* buf, int size) = 0;
    virtual size_t get_id3v1_tag(unsigned char* buf, size_t size) = 0;
    virtual size_t get_id3v2_tag(unsigned char* buf, size_t size) = 0;
    virtual size_t get_lametag_frame(unsigned char* buf, size_t size) = 0;
};

std::unique_ptr<ilame> make_lame_interface();

}
}

#endif
