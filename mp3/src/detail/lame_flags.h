#ifndef INCLUDED_MP3_DETAIL_LAME_FLAGS_H
#define INCLUDED_MP3_DETAIL_LAME_FLAGS_H

#include <lame.h>

namespace mp3 {
namespace detail {

class lame_flags
{
public:
    lame_flags();
    ~lame_flags();
    lame_global_flags* flags() const;
private:
    lame_global_flags* flags_;
};

}
}

#endif
