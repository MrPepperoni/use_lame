#include "ilame.h"
#include "lame_flags.h"

#include <lame.h>

namespace mp3 {
namespace detail {

namespace {
class lame_impl : public ilame {
    lame_flags flags_;

    int set_num_channels(int channels) override
    {
        return lame_set_num_channels(flags_.flags(), channels);
    }

    int set_num_samples(unsigned long samples) override
    {
        return lame_set_num_samples(flags_.flags(), samples);
    }

    int set_in_samplerate(int samplerate) override
    {
        return lame_set_in_samplerate(flags_.flags(), samplerate);
    }

    int set_brate(int brate) override
    {
        return lame_set_brate(flags_.flags(), brate);
    }

    int set_quality(int quality) override
    {
        return lame_set_quality(flags_.flags(), quality);
    }

    void set_write_id3tag_automatic(bool automatic) override
    {
        lame_set_write_id3tag_automatic(flags_.flags(), automatic ? 1 : 0);
    }

    int init_params() override
    {
        return lame_init_params(flags_.flags());
    }

    int encode_buffer_ieee_float(
        const float     pcm_l [],          /* PCM data for left channel     */
        const float     pcm_r [],          /* PCM data for right channel    */
        const int       nsamples,
        unsigned char * mp3buf,
        const int       mp3buf_size) override
    {
        return lame_encode_buffer_ieee_float(flags_.flags(),
                pcm_l,
                pcm_r,
                nsamples,
                mp3buf,
                mp3buf_size);
    }

    int encode_flush(unsigned char* buf, int size) override
    {
        return lame_encode_flush(flags_.flags(), buf, size);
    }

    size_t get_id3v1_tag(unsigned char* buf, size_t size) override
    {
        return lame_get_id3v1_tag(flags_.flags(), buf, size);
    }

    size_t get_id3v2_tag(unsigned char* buf, size_t size) override
    {
        return lame_get_id3v2_tag(flags_.flags(), buf, size);
    }

    size_t get_lametag_frame(unsigned char* buf, size_t size) override
    {
        return lame_get_lametag_frame(flags_.flags(), buf, size);
    }

};
}

ilame::~ilame() = default;

std::unique_ptr<ilame> make_lame_interface()
{
    return std::unique_ptr<ilame>(new lame_impl);
}

}
}

