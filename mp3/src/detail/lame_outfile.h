#ifndef INCLUDED_MP3_DETAIL_LAME_OUTFILE_H
#define INCLUDED_MP3_DETAIL_LAME_OUTFILE_H

#include <filesystem>
#include <stdio.h>

namespace mp3 {
namespace detail {

class lame_outfile
{
public:
    explicit lame_outfile(std::filesystem::path const& fname);
    ~lame_outfile();
    FILE* get() const;
private:
    FILE* file_;
};

}
}

#endif
