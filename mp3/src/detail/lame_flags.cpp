#include "lame_flags.h"
#include "lame_error.h"
#include <stdarg.h>

namespace mp3 {
namespace detail {

lame_flags::lame_flags()
    : flags_(lame_init())
{
    if (!flags_)
    {
        throw lame_error();
    }
}

lame_flags::~lame_flags()
{
    try {
        lame_close(flags_);
    } catch(...) {}
}

lame_global_flags* lame_flags::flags() const
{
    return flags_;
}

}
}
