#ifndef INCLUDED_MP3_DETAIL_LAME_CALL_H
#define INCLUDED_MP3_DETAIL_LAME_CALL_H

#include <vector>

namespace mp3 {
namespace detail {

template<typename buffer_type, typename F>
std::vector<buffer_type> lame_call(F lame_get_buffered_fun)
{
    auto req_size = lame_get_buffered_fun(nullptr, 0);
    std::vector<buffer_type> rv(req_size);
    auto written = lame_get_buffered_fun(rv.data(), rv.size());
    rv.resize(written);
    return rv;
}

}
}

#endif
