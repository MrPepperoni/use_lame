#ifndef INCLUDED_MP3_DETAIL_CONVERT_H
#define INCLUDED_MP3_DETAIL_CONVERT_H

#include "ilame.h"
#include <wav/iaudio_stream.h>
#include <stdio.h>

namespace mp3 {
namespace detail {

void convert(ilame& intf, wav::iaudio_stream& pcm, FILE* outfile);

}
}

#endif
