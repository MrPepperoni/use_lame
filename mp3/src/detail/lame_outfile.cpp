#include "lame_outfile.h"
#include "lame_error.h"

#include <type_traits>
#include <wchar.h>

template<typename c>
FILE* _wfopen(c const* fname, c const* openmode, typename std::enable_if<std::is_same<char, c>::value>::type* = 0)
{
    return fopen(fname, openmode);
}

template<typename char_type>
FILE* open_file(char_type const* fname, typename std::enable_if<std::is_same<char, char_type>::value>::type* = 0)
{
    return _wfopen(fname, "wb+");
}

template<typename char_type>
FILE* open_file(char_type const* fname, typename std::enable_if<std::is_same<wchar_t, char_type>::value>::type* = 0)
{
    return _wfopen(fname, L"wb+");
}

namespace mp3 {
namespace detail {

lame_outfile::lame_outfile(std::filesystem::path const& fname)
    : file_{open_file(fname.c_str())}
{
    if (!file_)
    {
        throw lame_error();
    }
}

lame_outfile::~lame_outfile()
{
    fclose(file_);
}

FILE* lame_outfile::get() const
{
    return file_;
}

}
}
