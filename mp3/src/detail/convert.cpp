#include "convert.h"

#include "lame_call.h"
#include "lame_error.h"

#include <algorithm>

namespace mp3 {
namespace detail {

namespace {
void throw_on_negative(int val)
{
    if (val < 0)
    {
        throw lame_error();
    }
}
}

void convert(ilame& intf, wav::iaudio_stream& pcm, FILE* outfile)
{
    auto const num_samples = pcm.remaining_samples();

    throw_on_negative(intf.set_num_channels(pcm.num_channels()));
    throw_on_negative(intf.set_in_samplerate(pcm.sample_rate()));
    throw_on_negative(intf.set_num_samples(pcm.remaining_samples()));
    throw_on_negative(intf.set_brate(320));
    throw_on_negative(intf.set_quality(2));
    intf.set_write_id3tag_automatic(0);

    throw_on_negative(intf.init_params());

    auto const& id3v2tag = detail::lame_call<unsigned char>(
            [&intf](unsigned char* buf, size_t s) {
                return intf.get_id3v2_tag(buf, s);
            });
    fwrite(id3v2tag.data(), sizeof(unsigned char), id3v2tag.size(), outfile);

    static const size_t samples_per_batch = 8 * 1024 * 1024;
    std::vector<unsigned char> mp3buf(1.25 * samples_per_batch + 7200);

    while (pcm.remaining_samples() != 0)
    {
        auto const num_samples = std::min(pcm.remaining_samples(), samples_per_batch);
        auto const& audio_data = pcm.read(num_samples);
        auto out_bytes = intf.encode_buffer_ieee_float(
                std::get<0>(audio_data).data(),
                std::get<1>(audio_data).data(),
                num_samples,
                mp3buf.data(),
                mp3buf.size());
        throw_on_negative(out_bytes);
        fwrite(mp3buf.data(), sizeof(unsigned char), out_bytes, outfile);
    }

    auto out_bytes = intf.encode_flush(mp3buf.data(), mp3buf.size());
    throw_on_negative(out_bytes);
    fwrite(mp3buf.data(), sizeof(unsigned char), out_bytes, outfile);

    auto const& id3v1tag = detail::lame_call<unsigned char>(
            [&intf](unsigned char* buf, size_t s) {
                return intf.get_id3v1_tag(buf, s);
            });
    fwrite(id3v1tag.data(), sizeof(unsigned char), id3v1tag.size(), outfile);

    auto const& lametag = detail::lame_call<unsigned char>(
            [&intf](unsigned char* buf, size_t s) {
                return intf.get_lametag_frame(buf, s);
            });
    fseek(outfile, id3v2tag.size(), SEEK_SET);
    fwrite(lametag.data(), sizeof(unsigned char), lametag.size(), outfile);
}

}
}


