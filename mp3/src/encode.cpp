#include "encode.h"
#include "detail/lame_outfile.h"
#include "detail/ilame.h"
#include "detail/convert.h"

namespace mp3 {

void convert(wav::iaudio_stream& pcm, std::filesystem::path const& filename)
{
    auto intf = detail::make_lame_interface();
    detail::lame_outfile ofs(filename);
    detail::convert(*intf, pcm, ofs.get());
}

}

