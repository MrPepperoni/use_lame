/** @file encode.h
 *  @brief contains functions to encode iaudio_stream to mp3
 */
#ifndef INCLUDED_MP3_ENCODE_H
#define INCLUDED_MP3_ENCODE_H

#include <wav/iaudio_stream.h>
#include <filesystem>

/** @brief mp3 namespace */
namespace mp3 {

/** @brief convert audio stream to mp3
 *  @param pcm iaudio_stream to encode
 *  @param filename name of the resulting mp3 file
 */
void convert(wav::iaudio_stream& pcm, std::filesystem::path const& filename);

}

#endif

