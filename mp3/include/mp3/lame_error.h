/** @file lame_error.h
 *  @brief contains exceptions concerning mp3 encoding
 */
#ifndef INCLUDED_MP3_LAME_ERROR_H
#define INCLUDED_MP3_LAME_ERROR_H

#include <exception>

/** @brief mp3 namespace */
namespace mp3 {
/** @brief Exception signalling an error during lame encoding */
class lame_error : public std::exception
{
public:
    /** @brief describe the error
     *  @return string describing the error
     */
    char const* what() const noexcept override;
};
}

#endif
