#include "../src/detail/convert.h"
#include "../src/detail/ilame.h"
#include <wav/iaudio_stream.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>

// todo ilame mock

struct mock_audio_stream : wav::iaudio_stream
{
    MOCK_METHOD(uint16_t, num_channels, (), (const, override));
    MOCK_METHOD(uint32_t, sample_rate, (), (const, override));
    MOCK_METHOD(size_t, remaining_samples, (), (const, override));
    MOCK_METHOD((std::tuple<std::vector<float>, std::vector<float>>), read, (size_t), (override));

    mock_audio_stream()
    {
    }
};

struct mock_lame : mp3::detail::ilame
{
    MOCK_METHOD(int, set_num_channels, (int), (override));
    MOCK_METHOD(int, set_num_samples, (unsigned long), (override));
    MOCK_METHOD(int, set_in_samplerate, (int), (override));
    MOCK_METHOD(int, set_brate, (int), (override));
    MOCK_METHOD(int, set_quality, (int), (override));
    MOCK_METHOD(void, set_write_id3tag_automatic, (bool), (override));
    MOCK_METHOD(int, init_params, (), (override));
    MOCK_METHOD(int, encode_buffer_ieee_float, (const float [], const float [], const int, unsigned char*, const int), (override));
    MOCK_METHOD(int, encode_flush, (unsigned char*, int), (override));
    MOCK_METHOD(size_t, get_id3v1_tag, (unsigned char*, size_t), (override));
    MOCK_METHOD(size_t, get_id3v2_tag, (unsigned char*, size_t), (override));
    MOCK_METHOD(size_t, get_lametag_frame, (unsigned char*, size_t), (override));
};

struct tmp_file
{
    tmp_file()
        : handle(tmpfile())
    {
    }
    ~tmp_file()
    {
        fclose(handle);
    }
    FILE* handle = nullptr;
};

TEST(TestConvert, success)
{
    tmp_file f;
    ASSERT_NE(f.handle, nullptr);
    using namespace ::testing;
    NiceMock<mock_audio_stream> pcm;
    uint16_t channels = 2;
    uint32_t samplerate = 44100;
    size_t remaining_samples = 8000;

    auto remaining_samples_impl = [&]() -> size_t {
        return remaining_samples;
    };

    auto read_impl = [&](size_t count) -> std::tuple<std::vector<float>, std::vector<float>> {
        EXPECT_LE(count, remaining_samples);
        remaining_samples -= count;
        return std::make_tuple(std::vector<float>(count, 0.0), std::vector<float>(count, 0.0));
    };

    ON_CALL(pcm, num_channels())
        .WillByDefault(Return(channels));
    ON_CALL(pcm, sample_rate())
        .WillByDefault(Return(samplerate));
    ON_CALL(pcm, remaining_samples())
        .WillByDefault(Invoke(remaining_samples_impl));
    ON_CALL(pcm, read(_))
        .WillByDefault(Invoke(read_impl));

    NiceMock<mock_lame> intf;

    mp3::detail::convert(intf, pcm, f.handle);

    EXPECT_EQ(remaining_samples, 0);
}
