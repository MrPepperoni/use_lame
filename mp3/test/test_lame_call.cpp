#include "../src/detail/lame_call.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

struct buffered_func {
    virtual ~buffered_func() = default;
    virtual int get_buffered_fun(unsigned char* buf, size_t s) = 0;
};

struct mock_buffered_func : buffered_func {
    MOCK_METHOD(int, get_buffered_fun, (unsigned char* buf, size_t s), (override));
};

TEST(TestLameCall, call_twice)
{
    using namespace ::testing;
    StrictMock<mock_buffered_func> mock;
    int const req_buf_size = 5;
    EXPECT_CALL(mock, get_buffered_fun)
        .Times(2)
        .WillRepeatedly(Return(req_buf_size));

    auto rv = mp3::detail::lame_call<unsigned char>([&mock](unsigned char* c, size_t s)
            { return mock.get_buffered_fun(c, s); });

    EXPECT_EQ(rv.size(), req_buf_size);
}
