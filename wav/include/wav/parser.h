/** @file parser.h
 *  @brief provides a funtion for parsing audio data from a file
 */
#ifndef INCLUDED_WAV_PARSER_H
#define INCLUDED_WAV_PARSER_H

#include <wav/iaudio_stream.h>
#include <memory>
#include <filesystem>

/** @brief wav namespace */
namespace wav
{

/** @brief parse audio data
 *
 *  @param wav_file path to the file to parse
 *  @return iaudio_stream to read the audio data
 */
std::unique_ptr<iaudio_stream> parse(std::filesystem::path const& wav_file);

}

#endif
