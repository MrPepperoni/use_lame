/** @file iaudio_stream.h
 *  @brief introduce a class representing an audio stream
 */
#ifndef INCLUDED_WAV_IAUDIO_STREAM_H
#define INCLUDED_WAV_IAUDIO_STREAM_H

#include <tuple>
#include <vector>
#include <stdint.h>
#include <stddef.h>

/** @brief wav namespace */
namespace wav {

/** @brief class representing an audio_stream */
class iaudio_stream
{
public:
    /** @brief enum identifying the audio channels */
    enum channel {
        channel_left = 0, //< left
        channel_right = 1, //< right
        max_channels_num = 2 //< number of supported channels
    };
    /** Destructor */
    virtual ~iaudio_stream();
    /** Return the number of channels in the file */
    virtual uint16_t num_channels() const = 0;
    /** Return the sampling rate of the audio data */
    virtual uint32_t sample_rate() const = 0;
    /** Return the number of unread samples */
    virtual size_t remaining_samples() const = 0;
    /** Read audio samples from the stream
     *
     * @param count number of frames to read
     * @return two vectors containing the read samples for each channel
     */
    virtual std::tuple<std::vector<float>, std::vector<float>> read(size_t count) = 0;
};

}

#endif
