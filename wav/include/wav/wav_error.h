/** @file wav_error.h
 *  @brief contains classes for representing errors encountered during wav parsing
 */
#ifndef INCLUDED_WAV_WAV_ERROR_H
#define INCLUDED_WAV_WAV_ERROR_H

#include <exception>

/** @brief wav namespace */
namespace wav {

/** @brief class for representing an input error */
class input_error : public std::exception
{
public:
    /** @brief describe the error
     *  @return string describing the error
     */
    const char* what() const noexcept override;
};

/** @brief class for signalling an unsupported audio format */
class format_error : public std::exception
{
public:
    /** @brief describe the error
     *  @return string describing the error
     */
    const char* what() const noexcept override;
};

}

#endif
