#include "wav/parser.h"
#include "wav/wav_error.h"
#include <binio/binio.h>
#include "../src/detail/data.h"
#include <gtest/gtest.h>
#include <sstream>
#include <array>
#include <string>

template<size_t n>
std::ostream& operator<<(std::ostream& out, std::array<uint8_t, n> const& byte_arr)
{
    for (auto const& b: byte_arr)
    {
        out.put(b);
    }
    return out;
}

namespace wav {
namespace detail {
bool operator==(header const& lhs, header const& rhs)
{
    return lhs.wave_data_size_bytes == rhs.wave_data_size_bytes;
}

bool operator==(chunk const& lhs, chunk const& rhs)
{
    return lhs.data_size_bytes == rhs.data_size_bytes;
}
}
}

TEST(AlignStream, no_op)
{
    std::stringstream strm;

    auto loc = strm.tellg();
    auto aligned_loc = wav::detail::align_stream(strm).tellg();
    EXPECT_EQ(loc, aligned_loc);
    EXPECT_EQ(aligned_loc % 2, 0);
}

TEST(AlignStream, success_noop)
{
    std::array<uint8_t, 4> riff{
        0x61, 0x6e, 0x79, 0x00 // 'any '
    };

    std::stringstream strm;
    strm << riff;
    auto c = strm.get();
    EXPECT_EQ(c, 0x61);

    auto loc = strm.tellg();
    auto aligned_loc = wav::detail::align_stream(strm).tellg();
    auto diff = aligned_loc - loc;
    EXPECT_EQ(diff, 1);
    EXPECT_EQ(aligned_loc % 2, 0);
}

TEST(AlignStream, success)
{
    std::array<uint8_t, 6> riff{
        0xff, 0x00, 0x61, 0x6e, 0x79, 0x40 // 'any '
    };

    std::stringstream strm;
    strm << riff;
    auto c = strm.get();
    EXPECT_EQ(c, 0xff);

    auto loc = strm.tellg();
    auto aligned_loc = wav::detail::align_stream(strm).tellg();
    auto diff = aligned_loc - loc;
    auto val = binio::u32(strm);
    EXPECT_EQ(diff, 1);
    EXPECT_EQ(aligned_loc % 2, 0);
    EXPECT_EQ(val, 0x40796e61);
}

TEST(ReadChunkId, success)
{
    std::array<uint8_t, 4> riff{
        0x61, 0x6e, 0x79, 0x00 // 'any '
    };

    std::stringstream strm;
    strm << riff;

    auto rv = wav::detail::read_chunk_id(strm);
    wav::detail::chunk_id expected{ 0x61, 0x6e, 0x79, 0x00 };
    EXPECT_EQ(rv, expected);
}

TEST(ReadChunkId, exception_empty)
{
    std::stringstream strm;

    ASSERT_THROW(wav::detail::read_chunk_id(strm), std::iostream::failure);
}

TEST(ReadChunkId, exception_incomplete)
{
    std::array<uint8_t, 3> riff{
        0x61, 0x6e, 0x79 // 'any'
    };

    std::stringstream strm;
    strm << riff;

    ASSERT_THROW(wav::detail::read_chunk_id(strm), std::iostream::failure);
}

TEST(ReadChunk, exception_empty)
{
    std::stringstream strm;

    ASSERT_THROW(wav::detail::read_chunk(strm), std::iostream::failure);
}

TEST(ReadChunk, exception_incomplete)
{
    std::array<uint8_t, 3> riff{
        0x61, 0x6e, 0x79
    };

    std::stringstream strm;
    strm << riff;

    ASSERT_THROW(wav::detail::read_chunk(strm), std::iostream::failure);
}

TEST(ReadChunk, success)
{
    std::array<uint8_t, 4> riff{
        0x40, 0x1f, 0x00, 0x00
    };

    std::stringstream strm;
    strm << riff;

    auto rv = wav::detail::read_chunk(strm);
    EXPECT_EQ(rv.data_size_bytes, 0x1f40);
}

TEST(ReadHeader, success)
{
    std::array<uint8_t, 12> riff{
        0x52, 0x49, 0x46, 0x46, // RIFF
        0x04, 0x00, 0x00, 0x00, // filesize, LE
        0x57, 0x41, 0x56, 0x45  // WAVE
    };

    std::stringstream strm;
    strm << riff;

    auto rv = wav::detail::read_header(strm);
    EXPECT_EQ(rv, wav::detail::header{0});
}

TEST(ReadHeader, exception_empty)
{
    std::stringstream strm;

    ASSERT_THROW(wav::detail::read_header(strm), std::iostream::failure);
}

TEST(ReadHeader, exception_incomplete)
{
    std::array<uint8_t, 8> riff{
        0x52, 0x49, 0x46, 0x46, // RIFF
        0x04, 0x00, 0x00, 0x00, // filesize, LE
    };

    std::stringstream strm;
    strm << riff;

    ASSERT_THROW(wav::detail::read_header(strm), wav::input_error);
}

TEST(ReadHeader, exception_not_riff)
{
    std::array<uint8_t, 12> riff{
        0x52, 0x55, 0x46, 0x46, // RUFF
        0x04, 0x00, 0x00, 0x00, // filesize, LE
        0x57, 0x41, 0x56, 0x45  // WAVE
    };

    std::stringstream strm;
    strm << riff;

    ASSERT_THROW(wav::detail::read_header(strm), wav::input_error);
}

TEST(ReadHeader, exception_not_wave)
{
    std::array<uint8_t, 12> riff{
        0x52, 0x49, 0x46, 0x46, // RIFF
        0x04, 0x00, 0x00, 0x00, // filesize, LE
        0x57, 0x41, 0x56, 0x41  // WAVA
    };

    std::stringstream strm;
    strm << riff;

    ASSERT_THROW(wav::detail::read_header(strm), wav::input_error);
}

TEST(ReadHeader, exception_incorrect_filesize)
{
    std::array<uint8_t, 12> riff{
        0x52, 0x49, 0x46, 0x46, // RIFF
        0x08, 0x00, 0x00, 0x00, // filesize, LE
        0x57, 0x41, 0x56, 0x45  // WAVE
    };

    std::stringstream strm;
    strm << riff;

    ASSERT_THROW(wav::detail::read_header(strm), wav::input_error);
}

TEST(ReadFMT, exception_empty)
{
    std::stringstream strm;
    ASSERT_THROW(wav::detail::read_fmt(strm), std::iostream::failure);
}

TEST(ReadFMT, exception_incomplete)
{
    std::array<uint8_t, 4> chk{
        0x10, 0x00, 0x00, 0x00  // data_size
    };

    std::stringstream strm;
    strm << chk;
    ASSERT_THROW(wav::detail::read_fmt(strm), std::iostream::failure);
}

TEST(ReadFMT, success_pcm)
{
    std::array<uint8_t, 4> chk{
        0x10, 0x00, 0x00, 0x00  // data_size
    };

    std::array<uint8_t, 16> pcm{
        0x01, 0x00, // 1 = pcm
        0x02, 0x00, // num_channels
        0x44, 0xac, 0x00, 0x00,  // sample_rate
        0x57, 0x41, 0x56, 0x41,  // avg_bytes_per_sec
        0x10, 0x00, // block_align
        0x10, 0x00 // significant_bits_per_sample
    };

    std::stringstream strm;
    strm << chk << pcm;
    auto fmt = wav::detail::read_fmt(strm);
    EXPECT_EQ(fmt.format_code, wav::detail::format::pcm);
    EXPECT_EQ(fmt.num_channels, 2);
    EXPECT_EQ(fmt.sample_rate, 44100);
    EXPECT_EQ(fmt.block_align, 16);
    EXPECT_EQ(fmt.bits_per_sample, 16);
    EXPECT_EQ(strm.peek(), std::char_traits<char>::eof());
}

TEST(ReadFMT, exception_pcm_no_bits_per_sample)
{
    std::array<uint8_t, 4> chk{
        0x0e, 0x00, 0x00, 0x00  // data_size
    };

    std::array<uint8_t, 14> pcm{
        0x01, 0x00, // 1 = pcm
        0x02, 0x00, // num_channels
        0x44, 0xac, 0x00, 0x00,  // sample_rate
        0x57, 0x41, 0x56, 0x41,  // avg_bytes_per_sec
        0x10, 0x00 // block_align
    };

    std::stringstream strm;
    strm << chk << pcm;
    ASSERT_THROW(wav::detail::read_fmt(strm), std::iostream::failure);
}

TEST(ReadFMT, success_unknown)
{
    std::array<uint8_t, 4> chk{
        0x0e, 0x00, 0x00, 0x00  // data_size
    };

    std::array<uint8_t, 14> pcm{
        0x00, 0x00, // 1 = pcm
        0x02, 0x00, // num_channels
        0x44, 0xac, 0x00, 0x00,  // sample_rate
        0x57, 0x41, 0x56, 0x41,  // avg_bytes_per_sec
        0x10, 0x00, // block_align
    };

    std::stringstream strm;
    strm << chk << pcm;
    auto fmt = wav::detail::read_fmt(strm);
    EXPECT_EQ(fmt.format_code, wav::detail::format::unknown);
    EXPECT_EQ(fmt.num_channels, 2);
    EXPECT_EQ(fmt.sample_rate, 44100);
    EXPECT_EQ(fmt.block_align, 16);
    EXPECT_EQ(strm.peek(), std::char_traits<char>::eof());
}

TEST(ReadFMT, success_unknown_large)
{
    std::array<uint8_t, 4> chk{
        0x1e, 0x00, 0x00, 0x00  // data_size
    };

    std::array<uint8_t, 30> pcm{
        0x00, 0x00, // 1 = pcm
        0x02, 0x00, // num_channels
        0x44, 0xac, 0x00, 0x00,  // sample_rate
        0x57, 0x41, 0x56, 0x41,  // avg_bytes_per_sec
        0x10, 0x00, // block_align
        0x10, 0x00,
        0x0c, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00
    };

    std::stringstream strm;
    strm << chk << pcm;
    auto fmt = wav::detail::read_fmt(strm);
    EXPECT_EQ(fmt.format_code, wav::detail::format::unknown);
    EXPECT_EQ(fmt.num_channels, 2);
    EXPECT_EQ(fmt.sample_rate, 44100);
    EXPECT_EQ(fmt.block_align, 16);
    EXPECT_EQ(strm.peek(), std::char_traits<char>::eof());
}
