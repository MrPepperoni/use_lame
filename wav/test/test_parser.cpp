#include "wav/wav_error.h"
#include "../src/detail/audio_stream.h"
#include "../src/detail/data.h"
#include <gtest/gtest.h>
#include <sstream>
#include <array>

template<size_t n>
std::ostream& operator<<(std::ostream& out, std::array<uint8_t, n> const& byte_arr)
{
    for (auto const& b: byte_arr)
    {
        out.put(b);
    }
    return out;
}

class TestAudioStreamF : public testing::Test
{
protected:
    std::unique_ptr<std::stringstream> strm{new std::stringstream};
};

wav::detail::audio_stream create_stream(std::unique_ptr<std::stringstream> strm)
{
    std::unique_ptr<std::istream> istrm(strm.release());
    return wav::detail::audio_stream(std::move(istrm));
}

TEST_F(TestAudioStreamF, empty)
{
    ASSERT_THROW(create_stream(std::move(strm)), std::exception);
}

TEST_F(TestAudioStreamF, riff_only)
{
    std::array<uint8_t, 12> riff{
        0x52, 0x49, 0x46, 0x46, // RIFF
        0x04, 0x00, 0x00, 0x00, // filesize, LE
        0x57, 0x41, 0x56, 0x45  // WAVE
    };

    *strm << riff;
    ASSERT_THROW(create_stream(std::move(strm)), std::exception);
}

TEST_F(TestAudioStreamF, success_fmt_data)
{
    std::array<uint8_t, 12> riff{
        0x52, 0x49, 0x46, 0x46, // RIFF
        0x24, 0x00, 0x00, 0x00, // filesize, LE
        0x57, 0x41, 0x56, 0x45  // WAVE
    };

    std::array<uint8_t, 4> fmt{
        0x66, 0x6d, 0x74, 0x20 // 'fmt '
    };

    std::array<uint8_t, 4> chk{
        0x10, 0x00, 0x00, 0x00  // data_size
    };

    std::array<uint8_t, 16> pcm{
        0x01, 0x00, // 1 = pcm
        0x02, 0x00, // num_channels
        0x44, 0xac, 0x00, 0x00,  // sample_rate
        0x57, 0x41, 0x56, 0x41,  // avg_bytes_per_sec
        0x04, 0x00, // block_align
        0x10, 0x00 // significant_bits_per_sample
    };

    std::array<uint8_t, 8> data{
        0x64, 0x61, 0x74, 0x61, // 'data'
        0x00, 0x00, 0x00, 0x00
    };

    *strm << riff << fmt << chk << pcm << data;

    auto audio_stream = create_stream(std::move(strm));
    EXPECT_EQ(audio_stream.num_channels(), 2);
    EXPECT_EQ(audio_stream.sample_rate(), 44100);
}

TEST_F(TestAudioStreamF, success_data_fmt)
{
    std::array<uint8_t, 12> riff{
        0x52, 0x49, 0x46, 0x46, // RIFF
        0x24, 0x00, 0x00, 0x00, // filesize, LE
        0x57, 0x41, 0x56, 0x45  // WAVE
    };

    std::array<uint8_t, 4> fmt{
        0x66, 0x6d, 0x74, 0x20 // 'fmt '
    };

    std::array<uint8_t, 4> chk{
        0x10, 0x00, 0x00, 0x00  // data_size
    };

    std::array<uint8_t, 16> pcm{
        0x01, 0x00, // 1 = pcm
        0x02, 0x00, // num_channels
        0x44, 0xac, 0x00, 0x00,  // sample_rate
        0x57, 0x41, 0x56, 0x41,  // avg_bytes_per_sec
        0x04, 0x00, // block_align
        0x10, 0x00 // significant_bits_per_sample
    };

    std::array<uint8_t, 8> data{
        0x64, 0x61, 0x74, 0x61, // 'data'
        0x00, 0x00, 0x00, 0x00,
    };


    *strm << riff << data << fmt << chk << pcm;

    auto audio_stream = create_stream(std::move(strm));
    EXPECT_EQ(audio_stream.num_channels(), 2);
    EXPECT_EQ(audio_stream.sample_rate(), 44100);
}

TEST_F(TestAudioStreamF, exception_inconsistent_bytes_per_sample)
{
    std::array<uint8_t, 12> riff{
        0x52, 0x49, 0x46, 0x46, // RIFF
        0x24, 0x00, 0x00, 0x00, // filesize, LE
        0x57, 0x41, 0x56, 0x45  // WAVE
    };

    std::array<uint8_t, 4> fmt{
        0x66, 0x6d, 0x74, 0x20 // 'fmt '
    };

    std::array<uint8_t, 4> chk{
        0x10, 0x00, 0x00, 0x00  // data_size
    };

    std::array<uint8_t, 16> pcm{
        0x01, 0x00, // 1 = pcm
        0x02, 0x00, // num_channels
        0x44, 0xac, 0x00, 0x00,  // sample_rate
        0x57, 0x41, 0x56, 0x41,  // avg_bytes_per_sec
        0x06, 0x00, // block_align
        0x10, 0x00 // significant_bits_per_sample
    };

    std::array<uint8_t, 8> data{
        0x64, 0x61, 0x74, 0x61, // 'data'
        0x00, 0x00, 0x00, 0x00
    };


    *strm << riff << fmt << chk << pcm << data;

    ASSERT_THROW(create_stream(std::move(strm)), wav::input_error);
}


