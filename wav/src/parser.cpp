#include "parser.h"
#include "detail/audio_stream.h"
#ifdef USE_SNDSTREAM_IMPL
#   include "detail/sndstream.h"
#endif
#include <fstream>

namespace wav {

std::unique_ptr<iaudio_stream> parse(std::filesystem::path const& wav_file)
{
    std::unique_ptr<std::istream> input(new std::ifstream(wav_file, std::ios_base::binary));
#ifdef USE_SNDSTREAM_IMPL
    return std::unique_ptr<iaudio_stream>(new detail::sndstream(std::move(input)));
#else
    return std::unique_ptr<iaudio_stream>(new detail::audio_stream(std::move(input)));
#endif
}

}
