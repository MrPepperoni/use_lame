#include "wav_error.h"

namespace wav {

const char* input_error::what() const noexcept
{
    return "invalid input file";
}

const char* format_error::what() const noexcept
{
    return "unsupported wave format";
}

}
