#ifndef INCLUDED_WAV_DETAIL_DATA_H
#define INCLUDED_WAV_DETAIL_DATA_H

#include <iostream>
#include <array>
#include <vector>
#include <memory>
#include <stdint.h>

namespace wav {
namespace detail {

using chunk_id = std::array<uint8_t, 4>;

static constexpr const chunk_id fmt_chunk_id = {0x66, 0x6d, 0x74, 0x20};
static constexpr const chunk_id data_chunk_id = {0x64, 0x61, 0x74, 0x61};
static constexpr const chunk_id riff_chunk_id = {0x52, 0x49, 0x46, 0x46};

struct chunk
{
    uint32_t data_size_bytes = 0;
};

struct header
{
    uint32_t wave_data_size_bytes = 0;
};

enum class format
{
    unknown = 0,
    pcm = 1,
    ms_adpcm = 2,
    a_law = 6,
    u_law = 7,
    ima_adpcm = 17,
    yamaha_adpcm = 20,
    gsm = 49,
    itu_adpcm = 64,
    mpeg = 80,
    experimental = 0xffff
};

struct fmt : public chunk
{
    format format_code = format::unknown;
    uint16_t num_channels = 0;
    uint32_t sample_rate = 0;
    uint32_t avg_bytes_per_sec = 0;
    uint16_t block_align = 0;
    uint16_t bits_per_sample = 0;
};

struct data_chunk
{
    std::streampos start = 0;
    uint32_t num_bytes = 0;
};

std::istream& align_stream(std::istream& in);

chunk_id read_chunk_id(std::istream& in);

chunk read_chunk(std::istream& in);

header read_header(std::istream& in);

fmt read_fmt(std::istream& in);

}
}

#endif

