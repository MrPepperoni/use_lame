#include "get_mandatory_chunks.h"
#include "parser.h"
#include "wav_error.h"
#include <optional>

namespace wav {
namespace detail {
namespace {
bool is_accepted_format(fmt const& fm)
{
    return fm.format_code == format::pcm &&
        fm.data_size_bytes == 16 &&
        (fm.num_channels == 1 || fm.num_channels == 2) &&
        fm.block_align != 0 &&
        fm.bits_per_sample != 0;
}
}

std::tuple<fmt, data_chunk> get_mandatory_chunks(std::istream& in)
{
    std::optional<fmt> fmt;
    std::optional<data_chunk> data;
    while ((!fmt || !data) && in.good())
    {
        auto const& id = read_chunk_id(in);
        bool const is_fmt = fmt_chunk_id == id;
        if (fmt.has_value() && is_fmt)
        {
            throw format_error();
        }
        bool const is_data = !is_fmt && data_chunk_id == id;
        if (data.has_value() && is_data)
        {
            throw format_error();
        }
        if (is_fmt)
        {
            fmt = read_fmt(in);
            if (!is_accepted_format(*fmt))
            {
                throw format_error();
            }
        }
        else if (is_data)
        {
            auto const& chk = read_chunk(in);
            auto data_pos = in.tellg();
            data = data_chunk{data_pos, chk.data_size_bytes};

            // if fmt is not yet found, skip the actual data
            if (!fmt)
            {
                in.ignore(chk.data_size_bytes);
            }
        }
        else
        {
            // skip unknown chunks
            auto const& chk = read_chunk(in);
            in.ignore(chk.data_size_bytes);
        }
    }

    if (!fmt || !data)
    {
        throw input_error();
    }

    return std::make_tuple(std::move(*fmt), std::move(*data));
}
}
}
