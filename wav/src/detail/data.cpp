#include "parser.h"
#include "wav_error.h"

#include "data.h"

#include <binio/exception_setter.h>
#include <binio/binio.h>
#include <cassert>

namespace wav {
namespace detail {

std::istream& align_stream(std::istream& in)
{
    binio::exception_setter es(in);
    // chunks are word-aligned
    if (in.tellg() & 1)
    {
        in.ignore(1);
    }
    return in;
}

chunk_id read_chunk_id(std::istream& in)
{
    return binio::array<4>(align_stream(in));
}

chunk read_chunk(std::istream& in)
{
    return { binio::u32(align_stream(in)) };
}

header read_header(std::istream& in)
{
    auto id = read_chunk_id(in);

    if (id != riff_chunk_id)
    {
        throw input_error();
    }

    auto chk = read_chunk(in);
    {
        binio::exception_setter es(in);
        auto current_pos = in.tellg();
        in.seekg(0, std::ios_base::end);
        auto end_pos = in.tellg();
        auto remaining = end_pos - current_pos;
        in.seekg(current_pos);
        if (remaining != chk.data_size_bytes)
        {
            throw input_error();
        }
        // the WAVE tag cannot be included if data size is less than 4 bytes
        if (chk.data_size_bytes < 4)
        {
            throw input_error();
        }
    }

    auto wave_tag = binio::array<4>(in);

    if (wave_tag != std::array<uint8_t, 4>{0x57, 0x41, 0x56, 0x45})
    {
        throw input_error();
    }

    return header{chk.data_size_bytes - 4};
}

fmt read_fmt(std::istream& in)
{
    fmt rv;
    rv.data_size_bytes = read_chunk(in).data_size_bytes;

    auto chunk_start = in.tellg();
    if (rv.data_size_bytes < 14)
    {
        throw input_error();
    }

    rv.format_code = static_cast<format>(binio::u16(in));
    rv.num_channels = binio::u16(in);
    rv.sample_rate = binio::u32(in);
    rv.avg_bytes_per_sec = binio::u32(in);
    rv.block_align = binio::u16(in);

    // format specific data
    if (rv.format_code == format::pcm)
    {
        rv.bits_per_sample = binio::u16(in);
    }

    auto bytes_read = in.tellg() - chunk_start;
    if (bytes_read < rv.data_size_bytes)
    {
        in.ignore(rv.data_size_bytes - bytes_read);
    }

    return rv;
}

}
}
