#ifndef INCLUDED_WAV_DETAIL_SNDSTREAM_H
#define INCLUDED_WAV_DETAIL_SNDSTREAM_H

#include "iaudio_stream.h"
#include <binio/exception_setter.h>
#include <exception>
#include <memory>
#include <iostream>
#include <sndfile.h>

namespace wav {
namespace detail {

class sndstream : public iaudio_stream
{
public:
    explicit sndstream(std::unique_ptr<std::istream> input);
    ~sndstream();
    uint16_t num_channels() const override;
    uint32_t sample_rate() const override;
    size_t remaining_samples() const override;
    std::tuple<std::vector<float>, std::vector<float>> read(size_t count) override;

    sf_count_t get_filelen();
    sf_count_t seek(sf_count_t offset, int whence);
    sf_count_t read(void* ptr, sf_count_t count);
    sf_count_t write(void const* ptr, sf_count_t count);
    sf_count_t tell();
private:
    std::unique_ptr<std::istream> in_;
    binio::exception_setter es_;
    SF_VIRTUAL_IO virtio_{0};
    SF_INFO sfinfo_{0};
    SNDFILE* sndfile_ = nullptr;
    size_t frames_left_ = 0;
    size_t channel_left_ = 0;
    size_t channel_right_ = 1;
    std::exception_ptr exception_ = nullptr;
};

}
}

#endif

