#include "audio_stream.h"
#include "wav_error.h"
#include "data.h"
#include "fill_pcm_data.h"
#include "get_mandatory_chunks.h"
#include <cassert>

namespace wav {
namespace detail {

audio_stream::audio_stream(std::unique_ptr<std::istream> input)
    : in_{std::move(input)}
{
    assert(in_.get());
    auto const& hdr = read_header(*in_);

    fmt frmt;
    data_chunk data;

    std::tie(frmt, data) = get_mandatory_chunks(*in_);

    if (in_->tellg() != data.start)
    {
        in_->seekg(data.start);
    }

    auto bits_per_sample = frmt.bits_per_sample;
    auto bytes_per_sample = bits_per_sample / 8 + (bits_per_sample % 8 ? 1 : 0);

    if (bytes_per_sample * frmt.num_channels != frmt.block_align ||
        bytes_per_sample > 8)
    {
        throw input_error();
    }

    bytes_per_sample_ = bytes_per_sample;
    num_channels_ = frmt.num_channels;
    sample_rate_ = frmt.sample_rate;
    num_samples_ = data.num_bytes / frmt.block_align;
}

uint16_t audio_stream::num_channels() const
{
    return num_channels_;
}

uint32_t audio_stream::sample_rate() const
{
    return sample_rate_;
}

size_t audio_stream::remaining_samples() const
{
    return num_samples_;
}

std::tuple<std::vector<float>, std::vector<float>> audio_stream::read(size_t count)
{
    assert(count <= num_samples_);
    num_samples_ -= count;
    return detail::fill_pcm_data(*in_, num_channels_, count, bytes_per_sample_);
}

}
}

