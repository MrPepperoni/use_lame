#include "fill_pcm_data.h"
#include <binio/read_signed_t.h>
#include <cassert>
#include <limits>

namespace wav {
namespace detail {

template<typename T>
void read_signed_vec(std::istream& in,
        std::vector<float>& left,
        std::vector<float>& right,
        size_t num_channels,
        size_t num_frames,
        size_t bytes_per_sample)
{
    using st = std::make_signed_t<T>;
    double const scale = std::numeric_limits<st>::max();
    assert(scale > 0);
    for (size_t i = 0, max = num_frames * num_channels; i != max; ++i)
    {
        auto val = binio::read_signed<st>(in, bytes_per_sample);
        auto& target = ((num_channels == 1) || (i % 2 == 0)) ? left : right;
        target.push_back(val / scale);
    }
}

void read_uint8_vec(std::istream& in,
        std::vector<float>& left,
        std::vector<float>& right,
        size_t num_channels,
        size_t num_frames)
{
    double const mid = 128;
    double const scale = 255 - mid;
    assert(scale > 0);
    for (size_t i = 0, max = num_frames * num_channels; i != max; ++i)
    {
        auto val = binio::u8(in);
        auto& target = ((num_channels == 1) || (i % 2 == 0)) ? left : right;
        target.push_back((val - mid) / scale);
    }
}


std::tuple<std::vector<float>, std::vector<float>> fill_pcm_data(std::istream& in, uint16_t num_channels, size_t num_frames, size_t bytes_per_sample)
{
    std::vector<float> rv[2];

    assert(num_channels > 0);
    assert(num_channels < 3);
    assert(bytes_per_sample > 0);
    assert(bytes_per_sample < 9);

    rv[0].reserve(num_frames);
    if (num_channels == 2)
    {
        rv[1].reserve(num_frames);
    }

    switch (bytes_per_sample)
    {
    case 1:
        read_uint8_vec(in, rv[0], rv[1], num_channels, num_frames);
        break;
    case 2:
        read_signed_vec<int16_t>(in, rv[0], rv[1], num_channels, num_frames, bytes_per_sample);
        break;
    case 3:
    case 4:
        read_signed_vec<int32_t>(in, rv[0], rv[1], num_channels, num_frames, bytes_per_sample);
        break;
    default:
        read_signed_vec<int64_t>(in, rv[0], rv[1], num_channels, num_frames, bytes_per_sample);
        break;
    }

    return make_tuple(std::move(rv[0]), std::move(rv[1]));
}

}
}

