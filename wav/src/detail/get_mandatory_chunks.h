#ifndef INCLUDED_WAV_DETAIL_GET_MANDATORY_CHUNKS_H
#define INCLUDED_WAV_DETAIL_GET_MANDATORY_CHUNKS_H

#include "data.h"
#include <iostream>

namespace wav {
namespace detail {

std::tuple<fmt, data_chunk> get_mandatory_chunks(std::istream& in);

}
}

#endif
