#ifndef INCLUDED_WAV_DETAIL_FILL_PCM_DATA_H
#define INCLUDED_WAV_DETAIL_FILL_PCM_DATA_H

#include "parser.h"
#include <tuple>
#include <vector>
#include <iostream>
#include <stdint.h>

namespace wav {
namespace detail {
std::tuple<std::vector<float>, std::vector<float>> fill_pcm_data(std::istream& in, uint16_t num_channels, size_t num_frames, size_t bytes_per_sample);
}
}

#endif
