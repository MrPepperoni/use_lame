#ifndef INCLUDED_WAV_AUDIO_STREAM_H
#define INCLUDED_WAV_AUDIO_STREAM_H

#include "wav/iaudio_stream.h"
#include <memory>
#include <iostream>

namespace wav {
namespace detail {

class audio_stream : public iaudio_stream
{
public:
    explicit audio_stream(std::unique_ptr<std::istream> input);
    uint16_t num_channels() const override;
    uint32_t sample_rate() const override;
    size_t remaining_samples() const override;
    std::tuple<std::vector<float>, std::vector<float>> read(size_t count) override;
private:
    uint16_t num_channels_ = 0;
    uint16_t bytes_per_sample_ = 0;
    uint32_t sample_rate_ = 0;
    size_t num_samples_ = 0;
    std::unique_ptr<std::istream> in_;
};

}
}

#endif
