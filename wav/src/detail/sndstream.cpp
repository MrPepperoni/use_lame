#include "sndstream.h"
#include "wav_error.h"
#include <map>
#include <algorithm>
#include <cassert>

namespace wav {
namespace detail {
namespace {

sf_count_t sf_getfilelen(void* userdata)
{
    return static_cast<sndstream*>(userdata)->get_filelen();
}

sf_count_t sf_seek(sf_count_t offset, int whence, void* userdata)
{
    return static_cast<sndstream*>(userdata)->seek(offset, whence);
}

sf_count_t sf_read(void* ptr, sf_count_t count, void* userdata)
{
    return static_cast<sndstream*>(userdata)->read(ptr, count);
}

sf_count_t sf_write(void const* ptr, sf_count_t count, void* userdata)
{
    return static_cast<sndstream*>(userdata)->write(ptr, count);
}

sf_count_t sf_tell(void* userdata)
{
    return static_cast<sndstream*>(userdata)->tell();
}

SF_VIRTUAL_IO virt_io()
{
    return SF_VIRTUAL_IO{sf_getfilelen, sf_seek, sf_read, sf_write, sf_tell};
}

}

sndstream::sndstream(std::unique_ptr<std::istream> input)
    : in_(std::move(input))
    , es_(*in_)
    , virtio_(virt_io())
    , sndfile_(sf_open_virtual(&virtio_, SFM_READ, &sfinfo_, static_cast<void*>(this)))
    , frames_left_(sfinfo_.frames)
{
    if (exception_)
    {
        std::rethrow_exception(exception_);
    }
    if (!sndfile_)
    {
        throw input_error();
    }
    std::vector<int> channel_map(sfinfo_.channels, SF_CHANNEL_MAP_INVALID);
    std::vector<int> const left_channel_ids{SF_CHANNEL_MAP_LEFT, SF_CHANNEL_MAP_FRONT_LEFT, SF_CHANNEL_MAP_REAR_LEFT, SF_CHANNEL_MAP_FRONT_LEFT_OF_CENTER, SF_CHANNEL_MAP_SIDE_LEFT, SF_CHANNEL_MAP_TOP_FRONT_LEFT, SF_CHANNEL_MAP_TOP_REAR_LEFT};
    std::vector<int> const right_channel_ids{SF_CHANNEL_MAP_RIGHT, SF_CHANNEL_MAP_FRONT_RIGHT, SF_CHANNEL_MAP_REAR_RIGHT, SF_CHANNEL_MAP_FRONT_RIGHT_OF_CENTER, SF_CHANNEL_MAP_SIDE_RIGHT, SF_CHANNEL_MAP_TOP_FRONT_RIGHT, SF_CHANNEL_MAP_TOP_REAR_RIGHT};
    auto rv = sf_command(sndfile_, SFC_GET_CHANNEL_MAP_INFO, static_cast<void*>(channel_map.data()), sfinfo_.channels * sizeof(int));
    if (rv)
    {
        auto left_it = std::find_first_of(channel_map.begin(), channel_map.end(),
            left_channel_ids.begin(), left_channel_ids.end());
        auto right_it = std::find_first_of(channel_map.begin(), channel_map.end(),
            right_channel_ids.begin(), right_channel_ids.end());
        if (left_it != channel_map.end())
        {
            channel_left_ = left_it - channel_map.begin();
        }
        if (right_it != channel_map.end())
        {
            channel_right_ = right_it - channel_map.begin();
        }
    }
    if (exception_)
    {
        std::rethrow_exception(exception_);
    }
}

sndstream::~sndstream()
{
    sf_close(sndfile_);
}

uint16_t sndstream::num_channels() const
{
    return std::min<uint16_t>(sfinfo_.channels, 2);
}

uint32_t sndstream::sample_rate() const
{
    return sfinfo_.samplerate;
}

size_t sndstream::remaining_samples() const
{
    return frames_left_;
}

std::tuple<std::vector<float>, std::vector<float>> sndstream::read(size_t count)
{
    assert(count <= frames_left_);
    frames_left_ -= count;
    std::vector<float> l, r;
    l.reserve(count);
    r.reserve(sfinfo_.channels > 1 ? count : 0);
    std::vector<float> frames(count * sfinfo_.channels, 0.0f);

    auto rc = sf_readf_float(sndfile_, frames.data(), count);
    if (exception_)
    {
        std::rethrow_exception(exception_);
    }
    if (rc != count)
    {
        throw input_error();
    }

    for (size_t i = 0; i != count; ++i)
    {
        l.push_back(frames.at(channel_left_ + i * sfinfo_.channels));
        if (sfinfo_.channels > 1)
        {
            r.push_back(frames.at(channel_right_ + i * sfinfo_.channels));
        }
    }

    return std::make_tuple(std::move(l), std::move(r));
}

sf_count_t sndstream::get_filelen()
{
    try {
        auto pos = in_->tellg();
        in_->seekg(0, std::ios_base::end);
        auto rv = in_->tellg();
        in_->seekg(pos, std::ios_base::beg);
        return rv;
    } catch(...) {
        exception_ = std::current_exception();
        return 0;
    }
}

sf_count_t sndstream::seek(sf_count_t offset, int whence)
{
    try {
        static std::map<int, std::ios_base::seekdir> const seekm = {
            { SEEK_SET, std::ios_base::beg },
            { SEEK_END, std::ios_base::end },
            { SEEK_CUR, std::ios_base::cur }
        };
        in_->seekg(offset, seekm.at(whence));
        return 0;
    } catch(...) {
        exception_ = std::current_exception();
        return -1;
    }
}

sf_count_t sndstream::read(void* ptr, sf_count_t count)
{
    try {
        in_->read(static_cast<char*>(ptr), count);
        return in_->gcount();
    } catch(...) {
        exception_ = std::current_exception();
        return 0;
    }
}

sf_count_t sndstream::write(void const* ptr, sf_count_t count)
{
    assert(false);
    return 0;
}

sf_count_t sndstream::tell()
{
    try {
        return in_->tellg();
    } catch(...) {
        exception_ = std::current_exception();
        return -1;
    }
}

}
}
