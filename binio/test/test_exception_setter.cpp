#include "binio/exception_setter.h"
#include <gtest/gtest.h>
#include <sstream>
#include <exception>

class TestExceptionSetter : public testing::Test
{
protected:
    std::stringstream strm_;
};

class TestExceptionSetterSetMask : public TestExceptionSetter, public testing::WithParamInterface<std::tuple<std::ios_base::iostate, std::ios_base::iostate>>
{
};

TEST_P(TestExceptionSetterSetMask, SetRestoreMask)
{
    auto const& masks = GetParam();
    auto init_mask = std::get<0>(masks);
    auto target_mask = std::get<1>(masks);
    strm_.exceptions(init_mask);
    {
        binio::exception_setter es(strm_, target_mask);
        EXPECT_EQ(strm_.exceptions(), target_mask);
    }
    EXPECT_EQ(strm_.exceptions(), init_mask);
}

INSTANTIATE_TEST_SUITE_P(TestExceptionSetterSetRestoreMask, TestExceptionSetterSetMask,
    testing::Combine(
        testing::Values(
            std::ios_base::goodbit,
            std::ios_base::eofbit,
            std::ios_base::failbit,
            std::ios_base::badbit,
            std::ios_base::eofbit | std::ios_base::failbit,
            std::ios_base::failbit | std::ios_base::badbit,
            std::ios_base::badbit | std::ios_base::failbit | std::ios_base::eofbit
            ),
        testing::Values(
            std::ios_base::goodbit,
            std::ios_base::eofbit,
            std::ios_base::failbit,
            std::ios_base::badbit,
            std::ios_base::eofbit | std::ios_base::failbit,
            std::ios_base::failbit | std::ios_base::badbit,
            std::ios_base::badbit | std::ios_base::failbit | std::ios_base::eofbit
            )
    ));
