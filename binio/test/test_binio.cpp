#include "binio/binio.h"
#include <gtest/gtest.h>
#include <sstream>
#include <array>

template<size_t n>
struct Data
{
    std::array<uint8_t, n> data_le;
    unsigned expected;
};

template<size_t n>
std::ostream& operator<<(std::ostream& out, std::array<uint8_t, n> const& byte_arr)
{
    for (auto const& b: byte_arr)
    {
        out.put(b);
    }
    return out;
}

class BinioTest : public testing::Test
{
};

class BinioTestU8 : public BinioTest, public testing::WithParamInterface<Data<1>>
{
};

TEST_P(BinioTestU8, TestU8)
{
    auto const& val = GetParam();
    std::stringstream strm;
    strm << val.data_le;
    auto rv = binio::u8(strm);
    EXPECT_EQ(val.expected, rv);
}

INSTANTIATE_TEST_SUITE_P(U8Tests, BinioTestU8, testing::Values(
            Data<1>{{1}, 1},
            Data<1>{{2}, 2},
            Data<1>{{5}, 5},
            Data<1>{{0xff}, 0xff}
            ));

TEST(BinioTestU8Exception, ReadError)
{
    std::stringstream strm;
    ASSERT_THROW(binio::u8(strm), std::istream::failure);
}

class BinioTestU16 : public BinioTest, public testing::WithParamInterface<Data<2>>
{
};

TEST_P(BinioTestU16, TestU16)
{
    auto const& val = GetParam();
    std::stringstream strm;
    strm << val.data_le;
    auto rv = binio::u16(strm);
    EXPECT_EQ(val.expected, rv);
}

INSTANTIATE_TEST_SUITE_P(U16Tests, BinioTestU16, testing::Values(
            Data<2>{{0x01, 0x02}, 0x0201},
            Data<2>{{0x02, 0x00}, 0x0002},
            Data<2>{{0x03, 0x0f}, 0x0f03},
            Data<2>{{0xff, 0xff}, 0xffff}
            ));

TEST(BinioTestU16Exception, ReadErrorEmpty)
{
    std::stringstream strm;
    ASSERT_THROW(binio::u16(strm), std::istream::failure);
}

TEST(BinioTestU16Exception, ReadErrorPartial)
{
    std::stringstream strm;
    strm.put(0x03);
    ASSERT_THROW(binio::u16(strm), std::istream::failure);
}

class BinioTestU32 : public BinioTest, public testing::WithParamInterface<Data<4>>
{
};

TEST_P(BinioTestU32, TestU32)
{
    auto const& val = GetParam();
    std::stringstream strm;
    strm << val.data_le;
    auto rv = binio::u32(strm);
    EXPECT_EQ(val.expected, rv);
}

INSTANTIATE_TEST_SUITE_P(U32Tests, BinioTestU32, testing::Values(
            Data<4>{{0x01, 0x02, 0x03, 0x04}, 0x04030201},
            Data<4>{{0x02, 0x00, 0x00, 0x00}, 0x00000002},
            Data<4>{{0x03, 0x0f, 0x03, 0x04}, 0x04030f03},
            Data<4>{{0xff, 0xff, 0xff, 0xff}, 0xffffffff}
            ));

TEST(BinioTestU32Exception, ReadErrorEmpty)
{
    std::stringstream strm;
    ASSERT_THROW(binio::u32(strm), std::istream::failure);
}

TEST(BinioTestU32Exception, ReadErrorPartial)
{
    std::stringstream strm;
    strm.put(0x03);
    ASSERT_THROW(binio::u32(strm), std::istream::failure);
}


