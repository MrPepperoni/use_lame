#include "binio/read_signed_t.h"

#include <gtest/gtest.h>
#include <sstream>
#include <array>

template<size_t n>
std::ostream& operator<<(std::ostream& out, std::array<uint8_t, n> const& byte_arr)
{
    for (auto const& b: byte_arr)
    {
        out.put(b);
    }
    return out;
}

class ReadSignedT : public testing::Test
{
protected:
    std::stringstream strm_;
};

template<size_t n>
struct TestData
{
    std::array<uint8_t, n> data_le;
    int64_t expected;
    static constexpr const size_t num_bytes = n;
};

class ReadS2BytesF : public ReadSignedT, public testing::WithParamInterface<TestData<2>>
{
};

TEST_P(ReadS2BytesF, success)
{
    auto const& td = GetParam();
    strm_ << td.data_le;
    auto val = binio::read_signed<int16_t>(strm_, td.num_bytes);
    EXPECT_EQ(val, td.expected);
}

INSTANTIATE_TEST_SUITE_P(ReadS2Bytes, ReadS2BytesF, testing::Values(
            TestData<2>{{0x01,0x02}, 513},
            TestData<2>{{0xfe,0xff}, -2}
            ));

class ReadS3BytesF : public ReadSignedT, public testing::WithParamInterface<TestData<3>>
{
};

TEST_P(ReadS3BytesF, success)
{
    auto const& td = GetParam();
    strm_ << td.data_le;
    auto val = binio::read_signed<int32_t>(strm_, td.num_bytes);
    EXPECT_EQ(val, td.expected);
}

INSTANTIATE_TEST_SUITE_P(ReadS3Bytes, ReadS3BytesF, testing::Values(
            TestData<3>{{0x01, 0x02, 0x03}, 0x03020100},
            TestData<3>{{0x00, 0xfe, 0xff}, -131072}
            ));

class ReadS4BytesF : public ReadSignedT, public testing::WithParamInterface<TestData<4>>
{
};

TEST_P(ReadS4BytesF, success)
{
    auto const& td = GetParam();
    strm_ << td.data_le;
    auto val = binio::read_signed<int32_t>(strm_, td.num_bytes);
    EXPECT_EQ(val, td.expected);
}

INSTANTIATE_TEST_SUITE_P(ReadS4Bytes, ReadS4BytesF, testing::Values(
            TestData<4>{{0x01, 0x02, 0x03, 0x04}, 0x04030201},
            TestData<4>{{0x00, 0x00, 0xfe, 0xff}, -131072}
            ));

