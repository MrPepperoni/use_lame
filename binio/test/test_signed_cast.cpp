#include "binio/read_signed_t.h"

#include <gtest/gtest.h>
#include <type_traits>
#include <sstream>
#include <limits>
#include <array>
#include <stdint.h>

template<typename T>
struct TestData
{
    using st = std::make_signed_t<T>;
    using ut = std::make_unsigned_t<T>;
    st s;
    ut u;
};

struct TestSignedCast : public testing::Test
{
};

struct TestSignedCast8F : public TestSignedCast, public testing::WithParamInterface<TestData<uint8_t>>
{
};

TEST_P(TestSignedCast8F, test)
{
    auto const& p = GetParam();
    auto cast_signed = binio::signed_cast<int8_t>(p.u);
    auto cast_unsigned = binio::signed_cast<uint8_t>(p.s);
    EXPECT_EQ(cast_signed, p.s);
    EXPECT_EQ(cast_unsigned, p.u);
}

INSTANTIATE_TEST_SUITE_P(TestSignedCast8, TestSignedCast8F, testing::Values(
                TestData<uint8_t>{ 0, 0 },
                TestData<uint8_t>{ 127, 127 },
                TestData<uint8_t>{ -127, 129 },
                TestData<uint8_t>{ -12, 244 },
                TestData<uint8_t>{ -1, 255 }
            ));

struct TestSignedCast16F : public TestSignedCast, public testing::WithParamInterface<TestData<uint16_t>>
{
};

TEST_P(TestSignedCast16F, test)
{
    auto const& p = GetParam();
    auto cast_signed = binio::signed_cast<int16_t>(p.u);
    auto cast_unsigned = binio::signed_cast<uint16_t>(p.s);
    EXPECT_EQ(cast_signed, p.s);
    EXPECT_EQ(cast_unsigned, p.u);
}

INSTANTIATE_TEST_SUITE_P(TestSignedCast16, TestSignedCast16F, testing::Values(
                TestData<uint16_t>{ 0, 0 },
                TestData<uint16_t>{ 127, 127 },
                TestData<uint16_t>{ 32767, 32767 },
                TestData<uint16_t>{ -32767, 32769 },
                TestData<uint16_t>{ -1, 65535 },
                TestData<uint16_t>{ -127, 65409 }
            ));

struct TestSignedCast32F : public TestSignedCast, public testing::WithParamInterface<TestData<uint32_t>>
{
};

TEST_P(TestSignedCast32F, test)
{
    auto const& p = GetParam();
    auto cast_signed = binio::signed_cast<int32_t>(p.u);
    auto cast_unsigned = binio::signed_cast<uint32_t>(p.s);
    EXPECT_EQ(cast_signed, p.s);
    EXPECT_EQ(cast_unsigned, p.u);
}

INSTANTIATE_TEST_SUITE_P(TestSignedCast32, TestSignedCast32F, testing::Values(
                TestData<uint32_t>{ 0, 0 },
                TestData<uint32_t>{ 127, 127 },
                TestData<uint32_t>{ 32767, 32767 },
                TestData<uint32_t>{ 32769, 32769 },
                TestData<uint32_t>{ 65535, 65535 },
                TestData<uint32_t>{ 2147483647, 2147483647 },
                TestData<uint32_t>{ -2147483647, 2147483649 },
                TestData<uint32_t>{ -1, 4294967295 }
            ));

