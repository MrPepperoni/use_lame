#include "binio.h"
#include "signed_cast.h"
#include "exception_setter.h"
#include <cstring>

namespace binio
{

uint8_t u8(std::istream& in)
{
    exception_setter es(in);
    return signed_cast<uint8_t>(in.get());
}

uint16_t u16(std::istream& in)
{
    exception_setter es(in);
    auto b1 = signed_cast<uint16_t>(in.get());
    auto b2 = signed_cast<uint16_t>(in.get());
    return b1 | b2 << 8;
}

uint32_t u32(std::istream& in)
{
    exception_setter es(in);
    auto b1 = signed_cast<uint32_t>(in.get());
    auto b2 = signed_cast<uint32_t>(in.get());
    auto b3 = signed_cast<uint32_t>(in.get());
    auto b4 = signed_cast<uint32_t>(in.get());
    return b1 | b2 << 8 | b3 << 16 | b4 << 24;
}

}

