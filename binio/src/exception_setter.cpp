#include "exception_setter.h"

namespace binio
{

exception_setter::exception_setter(std::istream& strm, std::ios_base::iostate exception_mask)
    : strm_(strm)
    , iostate_(strm_.exceptions())
{
    strm_.exceptions(exception_mask);
}

exception_setter::~exception_setter()
{
    try {
        strm_.exceptions(iostate_);
    } catch(...) {}
}

}
