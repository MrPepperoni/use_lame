/** @file binio.h
 *  @brief functions for reading unsigned types from little-endian streams
 */
#ifndef INCLUDED_BINIO_BINIO_H
#define INCLUDED_BINIO_BINIO_H

#include <istream>
#include <stdint.h>
#include <array>
#include <algorithm>

/** @brief binio namespace */
namespace binio
{

/** read an uint8_t */
uint8_t u8(std::istream& in);

/** read an uint16_t */
uint16_t u16(std::istream& in);

/** read an uint32_t */
uint32_t u32(std::istream& in);

/** read an array of uint8_t-s */
template<size_t N>
std::array<uint8_t, N> array(std::istream& in)
{
    std::array<uint8_t, N> rv;
    std::generate(std::begin(rv), std::end(rv), [&in]() { return u8(in); });
    return rv;
}

}

#endif
