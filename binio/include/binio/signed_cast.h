/** @file signed_cast.h
 *  @brief functions for signed-unsigned integral type conversions
 *
 *  This contains the definitions for signed-unsigned
 *  integral type conversions keeping the same in-memory representations.
 */
#ifndef INCLUDED_BINIO_SIGNED_CAST_H
#define INCLUDED_BINIO_SIGNED_CAST_H

#include <type_traits>
#include <cstring>

/** @brief binio namespace */
namespace binio {

/** cast unsigned to signed integral */
template<typename T>
typename std::enable_if<std::is_integral<T>::value && std::is_signed<T>::value,T>::type
signed_cast(typename std::make_unsigned<T>::type v) {
    typename std::make_signed<T>::type s;
    std::memcpy(&s,&v,sizeof v);
    return s;
}

/** cast unsigned to signed integral */
template<typename T>
typename std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value,T>::type
signed_cast(typename std::make_signed<T>::type v) {
    typename std::make_unsigned<T>::type s;
    std::memcpy(&s,&v,sizeof v);
    return s;
}

}

#endif
