/** @file exception_setter.h
 *  @brief RAII class for setting istream exception mask
 */
#ifndef INCLUDED_BINIO_EXCEPTION_SETTER_H
#define INCLUDED_BINIO_EXCEPTION_SETTER_H

#include <iostream>

/** @brief binio namespace */
namespace binio
{

/** @brief RAII class for setting istream exceptions
 *
 * Manages the exception mask of an istream during its lifetime
 */
class exception_setter
{
public:
    /** @brief construct an exception_setter
     *
     * Create the RAII wrapper and set the exception mask to the given value
     * while storing the original exception mask
     *
     * @param strm the std::istream object whose exception mask is managed
     * @param exception_mask the target exception mask
     */
    exception_setter(std::istream& strm,
        std::ios_base::iostate exception_mask =
            std::ios_base::failbit | std::ios_base::badbit);
    /** @brief Destructor
     *
     * Try to restore the original exceptions
     */
    ~exception_setter();
private:
    std::istream& strm_; //< reference to the managed stream
    std::ios_base::iostate iostate_; //< the initial exception mask of the managed stream
};

}

#endif
