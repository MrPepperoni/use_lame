/** @file read_signed_t.h
 *  @brief functions for reading signed integral types
 */
#ifndef INCLUDED_BINIO_READ_SIGNED_T_H
#define INCLUDED_BINIO_READ_SIGNED_T_H

#include <binio/binio.h>
#include <binio/signed_cast.h>

#include <cassert>
#include <type_traits>
#include <limits>

/** @brief binio namespace */
namespace binio {

/** @brief reads a signed integer type containing num_bytes most significant bytes
 *
 * @param in input stream
 * @param num_bytes number of bytes used to store the most significant bytes of the integer
 */
template<typename T>
std::make_signed_t<T> read_signed(std::istream& in, size_t num_bytes)
{
    assert(sizeof(T) >= num_bytes);
    auto lshift = sizeof(T) - num_bytes;
    using si = std::make_signed_t<T>;
    using ui = std::make_unsigned_t<T>;
    ui temp = 0;
    for (size_t i = 0; i != num_bytes; ++i)
    {
        temp |= static_cast<ui>(binio::u8(in)) << (i + lshift) * 8;
    }
    return signed_cast<si>(temp);
}

}

#endif
