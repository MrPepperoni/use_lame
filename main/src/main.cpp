#include <wav/parser.h>
#include <threading/thread_pool.h>
#include <mp3/encode.h>
#include <iostream>
#include <filesystem>
#include <exception>
#include <regex>
#include <wchar.h>

namespace {

void convert_file(std::filesystem::path const& wav_path)
{
    auto pcm = wav::parse(wav_path);

    auto mp3_path = wav_path;
    mp3_path.replace_extension(".mp3");

    mp3::convert(*pcm, mp3_path);
}

template<typename char_type>
struct wav_re_helper {};

template<>
struct wav_re_helper<char>
{
    std::regex const re{"\\.[wW][aA][vV]$"};
};

template<>
struct wav_re_helper<wchar_t>
{
    std::wregex const re{L"\\.[wW][aA][vV]$"};
};

void print_usage(char const* fname)
{
    std::cerr << "Usage: " << fname << " path/to/directory" << std::endl;
}

}

int main(int argc, char const** argv) try
{
    if (argc != 2 || !std::filesystem::is_directory(argv[1]))
    {
        print_usage(argv[0]);
        return 1;
    }

    threading::thread_pool pool;
    std::vector<std::pair<std::filesystem::path, std::future<void>>> futures;

    wav_re_helper<std::filesystem::path::value_type> wav_re;
    std::match_results<std::filesystem::path::string_type::const_iterator> match;
    for (auto& p: std::filesystem::directory_iterator(argv[1]))
    {
        auto const& path = p.path();
        if (std::regex_match(path.extension().native(), match, wav_re.re))
        {
            futures.emplace_back(path, pool.execute(convert_file, path));
        }
    }

    for (auto& f: futures)
    {
        try
        {
            f.second.get();
        }
        catch(...)
        {
            std::cerr << "error processing " << f.first.string() << std::endl;
        }
    }

    return 0;
}
catch(...)
{
    print_usage(argv[0]);
    return 2;
}

