/** @file thread_pool.h
 *  @brief contains a class implementing a thread_pool
 */
#ifndef INCLUDED_THREADING_THREAD_POOL_H
#define INCLUDED_THREADING_THREAD_POOL_H

#include <condition_variable>
#include <future>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <type_traits>
#include <vector>

/** @brief threading namespace */
namespace threading {

/** @brief a simple thread pool implementation
 */
class thread_pool
{
public:
    /** @brief Create a thread pool
     *
     * @param thread_count the number of threads to create in the pool, defaults to std::thread::hardware_concurrency
     */
    thread_pool(size_t thread_count = std::thread::hardware_concurrency());
    /** @brief Destructor
     *
     * destroy the thread pool, initiate stopping the threads, wait for threads to finish
     */
    ~thread_pool();

    /** @brief Schedule executing a job
     *
     * @param func callable to execute
     * @param ...args arguments to pass to the function when executed
     * @return std::future containing the return value of the job
     */
    template<typename F, typename ...Args>
    auto execute(F func, Args&&... args);
private:
    class task_base
    {
    public:
        virtual ~task_base();
        virtual void operator()() = 0;
    };

    template<typename F>
    class task : public task_base
    {
    public:
        task(F&& func)
            : func_(std::forward<F>(func))
        {
        }
        void operator()() override
        {
            func_();
        }

    private:
        F func_;
    };

    template<typename F>
    std::unique_ptr<task_base> make_task(F&& f)
    {
        return std::unique_ptr<task_base>(new task<F>(std::forward<F>(f)));
    }

    std::vector<std::thread> threads_;
    std::queue<std::unique_ptr<task_base>> tasks_;
    std::mutex tasks_mutex_;
    std::condition_variable tasks_cv_;
    bool terminate_ = false;
};

template<typename F, typename ...Args>
auto thread_pool::execute(F func, Args&&... args)
{
    std::packaged_task<std::invoke_result_t<F, Args...>()> pkgd_task(
            [f = std::move(func), a = std::make_tuple(std::forward<Args>(args)...)]() mutable
            {
                return std::apply(std::move(f), std::move(a));
            }
        );
    auto rv_future = pkgd_task.get_future();

    {
        std::unique_lock<std::mutex> tasks_lock(tasks_mutex_);
        tasks_.emplace(make_task(std::move(pkgd_task)));
    }

    tasks_cv_.notify_one();

    return std::move(rv_future);
}
}

#endif
