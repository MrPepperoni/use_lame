#include "threading/thread_pool.h"
#include <gtest/gtest.h>
#include <chrono>
#include <exception>

class ConstructorTest : public testing::Test
{
};

class ConstructorWithParamTest : public ConstructorTest, public testing::WithParamInterface<size_t>
{
};

TEST(ConstructorTest, zero_argument_ctor)
{
    threading::thread_pool pool;
    EXPECT_EQ(1, 1);
}

TEST_P(ConstructorWithParamTest, single_argument)
{
    threading::thread_pool pool(GetParam());
    EXPECT_EQ(1, 1);
}

INSTANTIATE_TEST_SUITE_P(ValidThreadPools, ConstructorWithParamTest, testing::Range<size_t>(0,10));

class ExecuteTest : public testing::Test
{
protected:
    ::threading::thread_pool pool_{8};
};


int foo()
{
    return 3 + 2;
}

TEST_F(ExecuteTest, SimpleFunction)
{
    auto rv = pool_.execute(foo);
    EXPECT_EQ(rv.get(), 5);
}

TEST_F(ExecuteTest, SimpleLambda)
{
    auto rv = pool_.execute([]() -> int { return 3 * 3; });
    EXPECT_EQ(rv.get(), 9);
}

TEST_F(ExecuteTest, SimpleArgs)
{
    auto rv = pool_.execute([](int a, int b) -> int { return a + b; }, 3, 4);
    EXPECT_EQ(rv.get(), 3 + 4);
}

// MS implementation of std::packaged_task::get_future does not work if args or captures cannot be copied
// https://developercommunity.visualstudio.com/content/problem/108672/unable-to-move-stdpackaged-task-into-any-stl-conta.html
#ifndef _MSC_VER

TEST_F(ExecuteTest, UniquePtrCapture)
{
    auto rv = pool_.execute([capt = std::make_unique<int>(8)] () -> int { return *capt; });
    EXPECT_EQ(rv.get(), 8);
}

TEST_F(ExecuteTest, UniquePtrArg)
{
    auto uptr = std::make_unique<int>(8);
    auto rv = pool_.execute([] (std::unique_ptr<int> arg) -> int { return *arg; }, std::move(uptr));
    EXPECT_EQ(rv.get(), 8);
}


TEST_F(ExecuteTest, Complex)
{
    auto rv = pool_.execute(
        [capt = std::make_unique<int>(5)] (std::unique_ptr<int> arg) -> int { return *capt + *arg; },
        std::make_unique<int>(6));
    EXPECT_EQ(rv.get(), 5 + 6);
}

#endif

TEST_F(ExecuteTest, LongRunning)
{
    using namespace std::chrono_literals;
    auto start = std::chrono::high_resolution_clock::now();
    auto rv = pool_.execute([] { std::this_thread::sleep_for(200ms); });
    rv.get();
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed = end - start;
    EXPECT_GE(elapsed.count(), 200);
}

class MultipleTasksTestF : public ExecuteTest, public testing::WithParamInterface<size_t>
{
};

TEST_P(MultipleTasksTestF, MultipleTasks)
{
    size_t num_tasks = GetParam();

    std::vector<std::pair<std::future<int>, int>> futures;
    for (size_t i = 0; i != num_tasks; ++i)
    {
        futures.emplace_back(pool_.execute([](int a) { return a * a; }, i), i * i);
    }

    for (auto& fut : futures)
    {
        EXPECT_EQ(fut.first.get(), fut.second);
    }
}

INSTANTIATE_TEST_SUITE_P(MultipleTasksTest, MultipleTasksTestF, testing::Range<size_t>(0,100));

class ThreadsVsTasksTestF : public testing::TestWithParam<std::tuple<size_t, size_t>>
{
};

TEST_P(ThreadsVsTasksTestF, ThreadsVsTasks)
{
    auto const& param = GetParam();
    auto num_threads = std::max<size_t>(1, std::get<0>(param));
    auto num_tasks = std::get<1>(param);

    using namespace std::chrono_literals;

    std::chrono::high_resolution_clock::time_point start;

    {
        ::threading::thread_pool pool(num_threads);

        start = std::chrono::high_resolution_clock::now();

        for (size_t i = 0; i != num_tasks; ++i)
        {
            pool.execute([] { std::this_thread::sleep_for(20ms); });
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed = end - start;
    EXPECT_GE(elapsed.count(), 20 * num_tasks / num_threads);
}

INSTANTIATE_TEST_SUITE_P(ThreadsVsTasksTest, ThreadsVsTasksTestF, testing::Combine(
            testing::Range<size_t>(0,10),
            testing::Range<size_t>(0,10)
            ));

TEST_F(ExecuteTest, ThrowingTask)
{
    auto rv = pool_.execute([] { throw std::exception(); });
    ASSERT_THROW(rv.get(), std::exception);
}

