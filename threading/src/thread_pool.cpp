#include "thread_pool.h"
#include <algorithm>

namespace threading {

thread_pool::task_base::~task_base() = default;

thread_pool::thread_pool(size_t thread_count)
{
    thread_count = std::max<size_t>(1, thread_count);
    for (size_t i = 0; i != thread_count; ++i)
    {
        threads_.emplace_back(
            std::thread(
                [&]()
                {
                    std::unique_lock<std::mutex> tasks_lock(tasks_mutex_, std::defer_lock);

                    while (true)
                    {
                        tasks_lock.lock();
                        tasks_cv_.wait(
                                tasks_lock,
                                [&]() -> bool { return !tasks_.empty() || terminate_; }
                            );

                        if (terminate_ && tasks_.empty())
                        {
                            return;
                        }

                        auto task = std::move(tasks_.front());
                        tasks_.pop();
                        tasks_lock.unlock();

                        (*task)();
                    };
                }
            )
        );
    }
}

thread_pool::~thread_pool()
{
    try {
        terminate_ = true;
        tasks_cv_.notify_all();
        for (auto& thread: threads_)
        {
            thread.join();
        }
    } catch(...) {}
}
}

